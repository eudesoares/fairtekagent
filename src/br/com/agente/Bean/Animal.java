/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agente.Bean;
import java.util.Calendar;

/**
 * Beam referente ao animais a serem manipulados na rede e seus parametros (Utilizado para cadastros e registro de eventos)
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */
public class Animal {
    private short ID;
    private long RFID;
    private int id_primary;
    private byte dia;
    private byte QA;
    private byte QR;
    private Calendar date;
    private byte receipe;
    private long ultimoDosador;
    private byte excluido;

    /**
     * Pega o valor referente ao ID de 16bits de trafego na rede de dosadores.<br>
     * Parametro máximo da rede por limitação da memória e firmware do dosador é
     * 2000 cadastros de animais
     * @return the ID
     */
    public short getID() {
        return ID;
    }

    /**
     * Atribui o valor referente ao ID de 16bits de trafego na rede de dosadores.<br>
     * Parametro máximo da rede por limitação da memória e firmware do dosador é
     * 2000 cadastros de animais
     * @param ID the ID to set
     */
    public void setID(short ID) {
        this.ID = ID;
    }

    /**
     * Pega o valor referente ao código do brinco RFID.<br>
     * Valor do RFID ocupa 38bits do valor long
     * @return the RFID
     */
    public long getRFID() {
        return RFID;
    }

    /**
     * Atribui o valor referente ao código do brinco RFID.<br>
     * Valor do RFID ocupa 38bits do valor long
     * @param RFID the RFID to set
     */
    public void setRFID(long RFID) {
        this.RFID = RFID;
    }

    /**
     * Pega o valor referente ao dia atual de alimentação do animal.<br>
     * O dia do animal varia de 0 a 120 dias.
     * @return the dia
     */
    public byte getDia() {
        return dia;
    }

    /**
     * Atribui o valor referente ao dia atual de alimentação do animal.<br>
     *  O dia do animal varia de 0 a 120 dias.
     * @param dia the dia to set
     */
    public void setDia(byte dia) {
        this.dia = dia;
    }

    /**
     * Pega o valor referente ao quantidade alimentada do animal em número de dosagem.<br>
     * O número de dosagem é o numero de vezes que é liberado ração para o animal, levando em
     * consideração a constante de g/dose que especifica o valor em gramas de ração.
     * @return the QA
     */
    public byte getQA() {
        return QA;
    }

    /**
     * Atribui o valor referente ao quantidade alimentada do animal em número de dosagem.<br>
     * O número de dosagem é o numero de vezes que é liberado ração para o animal, levando em
     * consideração a constante de g/dose que especifica o valor em gramas de ração.
     * @param QA the QA to set
     */
    public void setQA(byte QA) {
        this.QA = QA;
    }

    /**
     * Pega o valor referente ao quantidade restante de alimentação do animal em número de dosagem.<br>
     * O número de dosagem é o numero de vezes que é liberado ração para o animal, levando em
     * consideração a constante de g/dose que especifica o valor em gramas de ração.
     * @return the QR
     */
    public byte getQR() {
        return QR;
    }

    /**
     * Atribui o valor referente ao quantidade restante de alimentação do animal em número de dosagem.<br>
     * O número de dosagem é o numero de vezes que é liberado ração para o animal, levando em
     * consideração a constante de g/dose que especifica o valor em gramas de ração.
     * 
     * @param QR the QR to set
     */
    public void setQR(byte QR) {
        this.QR = QR;
    }

    /**
     * Pega a data-hora do registro do evento de alimentação do animal.<br>
     * Este evento é o evento registrado pelo dosador durante a recepção 
     * de um registro advindo do dosador, pois este armazena os registro 
     * em sua memória interna.
     * @return the data
     */
    public Calendar getDate() {
        return date;
    }

    /**
     * Atribui a data-hora do registro do evento de alimentação do animal.<br>
     * Este evento é o evento registrado pelo dosador durante a recepção 
     * de um registro advindo do dosador, pois este armazena os registro 
     * em sua memória interna.
     * @param date the data to set
     */
    public void setDate(Calendar date) {
        this.date = date;
    }

    /**
     * Pega a receita ou Curva de alimentação que o animal segue.<br>
     * Este valor deve ser no máximo 10 devido ao espaço reservado na memória local do dosador.
     * o qual pode armazenar no máximo 10 curvas de alimentação com no máximo 120 dias.
     * @return the receita
     */
    public byte getReceipe() {
        return receipe;
    }

    /**
     * Atribui a receita ou Curva de alimentação que o animal segue.<br>
     * Este valor deve ser no máximo 10 devido ao espaço reservado na memória local do dosador.
     * o qual pode armazenar no máximo 10 curvas de alimentação com no máximo 120 dias.
     * @param receipe the receita to set
     */
    public void setReceipe(byte receipe) {
        this.receipe = receipe;
    }

    /**
     * Pega o ultimo dosador ao qual o animal teve acesso, ou ao qual o registro se refere.<br>
     * Este valor é o MAC address do dosador.
     * @return the ultimoDosador
     */
    public long getUltimoDosador() {
        return ultimoDosador;
    }

    /**
     * Atribui o ultimo dosador ao qual o animal teve acesso, ou ao qual o registro se refere.<br>
     *      * Este valor é o MAC address do dosador.
     * @param ultimoDosador the ultimoDosador to set
     */
    public void setUltimoDosador(long ultimoDosador) {
        this.ultimoDosador = ultimoDosador;
    }

    /**
     * Pega o status do animal, se ativo ou não.<br>
     * @return the excluido
     */
    public byte getExcluido() {
        return excluido;
    }

    /**
     * Atribui o status do animal, se ativo ou não.<br>
     * @param excluido the status to set
     */
    public void setExcluido(byte excluido) {
        this.excluido = excluido;
    }

    /**
     * Pega o valor da chave primária.<br>
     * @return the id_primary
     */
    public int getId_primary() {
        return id_primary;
    }

    /**
     * Atribui o valor da chave primária.<br>
     * @param id_primary the id_primary to set
     */
    public void setId_primary(int id_primary) {
        this.id_primary = id_primary;
    }
    
}
