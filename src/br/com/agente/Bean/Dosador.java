/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agente.Bean;

/**
 * Beam referente as informações dos dosadores (Utilizado para a varredura da rede)
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */
public class Dosador {
    private int ID;
    private long MAC;
    private String name;

    /**
     * Pega o ID da chave primária do dosador no BD, porem todos os tratamentos do BD
     * estão sendo feitos com o MAC addres.
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * Atribui o ID da chave primária do dosador no BD, porem todos os tratamentos do BD
     * estão sendo feitos com o MAC addres.
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * Pega o MAC address do dosador  é um identificador único, sendo o endereço fisico dos
     * da rede zigbee.
     * @return the MAC
     */
    public long getMAC() {
        return MAC;
    }

    /**
     * Atribui o MAC address do dosador  é um identificador único, sendo o endereço fisico dos
     * da rede zigbee.
     * @param MAC the MAC to set
     */
    public void setMAC(long MAC) {
        this.MAC = MAC;
    }

    /**
     * Pega o nome de identificação do dosador.
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Atribui o nome de identificação do dosador
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
