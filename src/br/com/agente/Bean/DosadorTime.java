/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agente.Bean;

/**
 * Beam referente aos tempos de operação do dosador. (Utilizado para a parametrização dos dosadores)
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */
public class DosadorTime {
    private long  dosador;
    private int ID;
    private short feed;
    private short feedBegin;
    private short motorFeed;    
    private short motorStop;
    private short motorReverse;       
    private short animalOut;   

    /**
     * Ta - Tempo entre alimentação<br>
     * Pega o tempo emtre cada dosagem de alimentação, deve ser maior que a soma dos tempos de acionamento do motor.<br>
     * @return the feed
     */
    public short getFeed() {
        return feed;
    }

    /**
     * Ta - Tempo entre alimentação<br>
     * Atribui o tempo emtre cada dosagem de alimentação, deve ser maior que a soma dos tempos de acionamento do motor.<br>
     * @param feed the feed to set
     */
    public void setFeed(short feed) {
        this.feed = feed;
    }

    /**
     * Ti - Tempo de inicio<br>
     * Pega o tempo entre a detecção do animal e o início da primeira dose, o acionamento do motor.<br>
     * @return the feedBegin
     */
    public short getFeedBegin() {
        return feedBegin;
    }

    /**
     * Ti - Tempo de inicio<br>
     * Atribui o tempo entre a detecção do animal e o início da primeira dose, o acionamento do motor.<br>
     * @param feedBegin the feedBegin to set
     */
    public void setFeedBegin(short feedBegin) {
        this.feedBegin = feedBegin;
    }

    /**
     * Td - Tempo de dosagem<br>
     * Pega o tempo que o motor deve ficar acionado, representando uma dose de ração.<br>
     * @return the motorFeed
     */
    public short getMotorFeed() {
        return motorFeed;
    }

    /**
     * Td - Tempo de dosagem<br>
     * Atribui o tempo que o motor deve ficar acionado, representando uma dose de ração.<br>
     * @param motorFeed the motorFeed to set
     */
    public void setMotorFeed(short motorFeed) {
        this.motorFeed = motorFeed;
    }

    /**
     * Tp - Tempo de pausa do motor<br>
     * Pega o Tempo de pausa do motro entre o sentido de dosagem e reversão do dosador.<br>
     * Como o ciclo de alimentação exige que o dosador alimente e volte o motor um pouco para tirar ração residual do dosador,
     * o tempo Tp serve para dar uma pausa entre a troca de sentido do motor.<br>
     * @return the motorStop
     */
    public short getMotorStop() {
        return motorStop;
    }

    /**     
     * Tp - Tempo de pausa do motor<br>
     * Atribui Tempo de pausa do motro entre o sentido de dosagem e reversão do dosador.<br>
     * Como o ciclo de alimentação exige que o dosador alimente e volte o motor um pouco para tirar ração residual do dosador,
     * o tempo Tp serve para dar uma pausa entre a troca de sentido do motor.<br>
     * @param motorStop the motorStop to set
     */
    public void setMotorStop(short motorStop) {
        this.motorStop = motorStop;
    }

    /**
     * Tr - Tempo de reversão<br>
     * Pega o tempo que o motor fica acionado no sentido reverso para retirar a ração residual
     * do dosador.
     * @return the motorReverse
     */
    public short getMotorReverse() {
        return motorReverse;
    }

    /**
     * Tr - Tempo de reversão<br>
     * Atribui o tempo que o motor fica acionado no sentido reverso para retirar a ração residual
     * do dosador.
     * @param motorReverse the motorReverse to set
     */
    public void setMotorReverse(short motorReverse) {
        this.motorReverse = motorReverse;
    }

    /**
     * Ts - Tempo de saída<br>
     * Pega o tempo que o animal deve se ausentar da antena de detecção para considera que houve o fim
     * da alimentação e evitar gerar muitos registros de eventos de alimentação.
     * @return the animalOut
     */
    public short getAnimalOut() {
        return animalOut;
    }

    /**
     * Ts - Tempo de saída<br>
     * Atribui o tempo que o animal deve se ausentar da antena de detecção para considera que houve o fim
     * da alimentação e evitar gerar muitos registros de eventos de alimentação.
     * @param animalOut the animalOut to set
     */
    public void setAnimalOut(short animalOut) {
        this.animalOut = animalOut;
    }

    /**
     * Retorna o Dosador ao qual os parametros se referem
     * @return the dosador
     */
    public long getDosador() {
        return dosador;
    }

    /**
     * Atribui p Dosador ao qual os parametros se referem
     * @param dosador the dosador to set
     */
    public void setDosador(long dosador) {
        this.dosador = dosador;
    }

    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID = ID;
    }
}
