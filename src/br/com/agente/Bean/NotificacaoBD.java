/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agente.Bean;

import br.com.agente.Enum.NotificationsEnum;

/**
 * Bean que possui as informações referente as notificações
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */
public class NotificacaoBD {
    private NotificationsEnum code;
    private String valor;

    /**
     * Retorna o Enum  do tipo {@link NotificationsEnum} para identificar o tratamento correto
     * @see NotificationsEnum
     * @return the code
     */
    public NotificationsEnum getCode() {
        return code;
    }

    /**     
     * Atribui o Enum  do tipo {@link NotificationsEnum} para identificar o tratamento correto
     * @see NotificationsEnum
     * @param code the code to set
     */
    public void setCode(NotificationsEnum code) {
        this.code = code;
    }

    /**     
     * Retorna a String  com as informações da notificação no formato JSON
     * @see NotificationsEnum
     * @return the valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * Atribui a String  com as informações da notificação no formato JSON
     * @see NotificationsEnum
     * @param valor the valor to set
     */
    public void setValor(String valor) {
        this.valor = valor;
    }
}
