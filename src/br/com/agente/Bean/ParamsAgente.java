/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agente.Bean;

/**
 *  Parametros de operação do Agente
 *  @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */
public class ParamsAgente {
    private int t_Notficacoes;
    private int t_Varredura;
    private int t_Alive;

    /**
     * Retorna o tempo que será usado entre a requisição de uma notificação.
     * @return the t_Notficacoes
     */
    public int getT_Notficacoes() {
        return t_Notficacoes;
    }

    /**
     * Atribui o tempo que será usado entre a requisição de uma notificação.
     * @param t_Notficacoes the t_Notficacoes to set
     */
    public void setT_Notficacoes(int t_Notficacoes) {
        this.t_Notficacoes = t_Notficacoes;
    }

    /**
     * Retorna o tempo que será usado para determinar de quanto em quanto tempo será feito as requisições dos dosadores.
     * @return the t_Varredura
     */
    public int getT_Varredura() {
        return t_Varredura;
    }

    /**     
     * Atribui o tempo que será usado para determinar de quanto em quanto tempo será feito as requisições dos dosadores.     
     * @param t_Varredura the t_Varredura to set
     */
    public void setT_Varredura(int t_Varredura) {
        this.t_Varredura = t_Varredura;
    }

    /**
     * Retorna o tempo que será usado para o agente notificar que esta ativo.
     * @return the t_Alive
     */
    public int getT_Alive() {
        return t_Alive;
    }

    /**
     * Atribui o tempo que será usado para o agente notificar que esta ativo.
     * @param t_Alive the t_Alive to set
     */
    public void setT_Alive(int t_Alive) {
        this.t_Alive = t_Alive;
    }
}
