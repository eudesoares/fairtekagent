/**
 * Contém a implementação dos Beans com as informações necessárias para cada utilização entre os módulos.<br>
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">jin.ss.ptu@gmail.com</a>
 */
package br.com.agente.Bean;