/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agente.Dao;

import br.com.agente.Bean.Animal;
import br.com.agente.Bean.DosadorTime;
import br.com.agente.Bean.Dosador;
import br.com.agente.Bean.NotificacaoBD;
import br.com.agente.Bean.Receipe;
import br.com.agente.Bean.DosadorStatus;
import br.com.agente.Bean.ParamsAgente;
import br.com.agente.Enum.NotificationsEnum;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.json.simple.JSONObject;

/**
 * Implementação dos Metodos de chamada das stored procedures do BD para fazer os SELECT, INSERT ou UPDATE,  
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a> e Diogo Patto
 */
public class ServiceDao {
	
    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;
    
    /**
     * Metodo constutor
     * @param obj .
     * @throws Exception .
     */
    public ServiceDao(JSONObject obj) throws Exception {
            try {
                this.conn = (Connection) MySqlConfig.getConnection(obj);
            } catch (Exception e) {
                throw new Exception("Erro: " + e.getMessage());
            }
    }
    
    /**
     * Insere um novo dosador no BD decorrente de uma notificação de novo dosador
     * @see ServiceDao#insereNotificacaoNovoDosador(java.lang.String) 
     * @see br.com.agente.Enum.NotificationsEnum#NDNOT
     * @param MAC Endereço do dosador a ser inserido
     * @return .
     * @throws SQLException .
     * @throws Exception .
     */
    public boolean insereDosador(String MAC) throws SQLException, Exception{ 
            String SQL = " call sp_insert_dosador(?) ";
            try {
                    ps = (PreparedStatement) conn.prepareStatement(SQL);
                    ps.setString(1,MAC);
                    rs = ps.executeQuery();

                    return true;
            } catch (SQLException sqle) {
                    throw new SQLException(sqle);
            } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
            }
    }  
    
    /**
     * Insere a notificação de novo dosador ({@link br.com.agente.Enum.NotificationsEnum#NDNOT})
     * @see ServiceDao#insereDosador(java.lang.String) 
     * @see br.com.agente.Enum.NotificationsEnum#NDNOT
     * @param MAC Endereço novo dosador
     * @return .
     * @throws Exception .
     */
    public boolean insereNotificacaoNovoDosador(String MAC) throws Exception {
            String SQL = " call sp_insert_notificacao_novo_dosador(?) ";
            try {
                    ps = (PreparedStatement) conn.prepareStatement(SQL);
                    ps.setString(1, String.format("{\"mac\":\"%s\"}", MAC));
                    rs = ps.executeQuery();
                    return true;
            } catch (SQLException sqle) {
                    throw new Exception(sqle);
            } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
            }
    }
    
    /**
     * Insere a notificação de finm de configuração de dosador
     * @param MAC Endereço do dosador a ser inserido
     * @param status status da requisição de configuração
     * @return .
     * @throws Exception  .
     */
    public boolean insereNotificacaoNovoDosadorFim(String MAC,String status) throws Exception {
        String SQL = " call sp_insert_notificacao_novo_dosador_fim(?,?) ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                ps.setString(1,MAC);
                ps.setString(2, String.format("{\"mac\":\"%s\",\"status\":\"%s\"}", MAC,status));
                rs = ps.executeQuery();
                return true;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }
    
    /**
     * Insere um novo evento de dosagem
     * @param animal Dados do animal a ser inserido na tabel ade eventos
     * @return .
     * @throws Exception . 
     */
    public boolean insereNovoEventoDosagem(Animal animal) throws Exception {

        String SQL = " call sp_insert_dosador_registro(?,?,?,?) ";
        try {
                Date utilDate = animal.getDate().getTime();			
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateForMySql = sdf.format(utilDate);
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                ps.setString(1, Long.toHexString(animal.getUltimoDosador()));   //Dosador
                ps.setInt(2, (int) animal.getID());     //ID
                ps.setInt(3, (int) animal.getQA());     //QA
                ps.setString(4, dateForMySql);   //Data
                rs = ps.executeQuery();
                return true;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }
    
    /**
     * Busca as notificações/requisições ainda não solucionadas pelo agente
     * @return .
     * @throws Exception .
     */
    public NotificacaoBD buscaNotificacaoAgente() throws Exception {
        String SQL = " call sp_select_notificacao_agente() ";
        NotificacaoBD not=new NotificacaoBD();
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                rs = ps.executeQuery();

                if (rs.next()) {
                        not.setCode(NotificationsEnum.findType(rs.getInt("code")));
                        not.setValor(rs.getString("valor"));
                }
                return not;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }
    
    /**
     * Retorna a lista dos animais ativos cadastrados no sistema
     * @return .
     * @throws Exception . 
     */
    public ArrayList<Animal> buscaAnimaisLista() throws Exception{
        ArrayList<Animal> animais = new ArrayList<>();
        String SQL = " call sp_lista_animais_com_dieta_dia() ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                rs = ps.executeQuery();
                while(rs.next()) {
                    Animal a=new Animal();
                    a.setID((short) rs.getInt("id_animal"));
                    a.setRFID(rs.getLong("rfid_animal"));
                    a.setDia((byte) rs.getInt("dia_animal"));
                    a.setQR((byte) rs.getInt("qr"));
                    a.setReceipe((byte) rs.getInt("id_dieta"));
                    animais.add(a);
                }
                return animais;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }
   
    /**
    * Busca os animais ativos no sistema ou que sofreram alterações ou adição.
    * @return Retorna a lista de animais.
    * @throws Exception .
    */
    public ArrayList<Animal> buscaAnimaisListaUpdate() throws Exception{
        ArrayList<Animal> animais = new ArrayList<>();
        String SQL = " call sp_lista_animais_update() ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                rs = ps.executeQuery();
                while(rs.next()) {
                    Animal a=new Animal();
                    a.setID((short) rs.getInt("id_animal"));
                    a.setId_primary(rs.getInt("id_primary"));
                    a.setExcluido((byte) rs.getInt("excluido_animal"));
                    a.setRFID(rs.getLong("rfid_animal"));
                    a.setDia((byte) rs.getInt("dia_animal"));
                    a.setQR((byte) rs.getInt("qr"));
                    a.setReceipe((byte) rs.getInt("id_dieta"));
                    animais.add(a);
                }
                return animais;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }
    
    /**
     * Busca a lista de receitas cadastradas para armazenar nos dosadores
     * @return Retorna a lista de receitas.
     * @throws Exception .
     */
    public ArrayList<Receipe> buscaReceitasLista() throws Exception {
        ArrayList<Receipe> receitas = new ArrayList<>();
        String SQL = " call sp_lista_dietas_dias() ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                rs = ps.executeQuery();
                while(rs.next()) {
                        Receipe r=new Receipe();
                        short[] data= new short[120];
                        int i=0;
                        r.setID((byte) rs.getInt("id_dieta"));
                        data[i++]= (short) rs.getInt("qr");
                        while(rs.next()&&(rs.getInt("id_dieta")==r.getID())&&i<120){
                            data[i++]= (short) rs.getInt("qr");
                        }
                        r.setQRdia(data);
                        receitas.add(r);
                }
                return receitas;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }
    
    /**
     * Busca a lista de receitas cadastradas no sistema que foram modificadas
     * @return Retorna a lista de receitas
     * @throws Exception .
     */
    public ArrayList<Receipe> buscaReceitasUpdate() throws Exception {
        ArrayList<Receipe> receitas = new ArrayList<>();
        String SQL = " call sp_lista_dietas_update() ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                rs = ps.executeQuery();
                while(rs.next()) {
                        Receipe r=new Receipe();
                        short[] data= new short[120];
                        int i=0;
                        r.setID((byte) rs.getInt("id_dieta"));
                        data[i++]= (short) rs.getInt("qr");
                        while(rs.next()&&(rs.getInt("id_dieta")==r.getID())&&i<120){
                            data[i++]= (short) rs.getInt("qr");
                        }
                        r.setQRdia(data);
                        receitas.add(r);
                }
                return receitas;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }

    /**
     * Busca os tempos de operação dos dosadores 
     * @param mac Endereço do dosador
     * @return Retorna o objeto com os dados
     * @throws Exception .
     */
    public DosadorTime buscaDosadorTempos(String mac) throws Exception {
        String SQL = " call sp_select_parametros_dosador(?) ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                ps.setString(1, mac);   //Dosador
                rs = ps.executeQuery();
                DosadorTime t=new DosadorTime();
                if(rs.next()) {
                    t.setFeedBegin(rs.getShort("pTi"));
                    t.setFeed(rs.getShort("pTd"));
                    t.setMotorFeed(rs.getShort("pTa"));
                    t.setMotorStop(rs.getShort("pTp"));
                    t.setMotorReverse(rs.getShort("pTr"));
                    t.setAnimalOut(rs.getShort("pTs"));
                    t.setMotorStop((short) rs.getInt("pTar"));
                }
                return t;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }
    
    /**
     * Busca a lista de dosadores em operação para o agente fazer a varredura da rede
     * @return Retorna a lista  de dosadores
     * @throws Exception .
     */
    public ArrayList<Dosador> buscaDosadoresLista() throws Exception {
        ArrayList<Dosador> dosadores = new ArrayList<>();
        String SQL = " call sp_lista_dosadores() ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                rs = ps.executeQuery();
                while(rs.next()) {
                        Dosador d=new Dosador();
                        d.setID((byte) rs.getInt("id_dosador"));
                        d.setMAC(Long.parseLong(rs.getString("mac_dosador"), 16));
                        dosadores.add(d);
                }
                return dosadores;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }

    /**
     * Insere o status de um dosador, a verificação da inserção na tabela de registro de status fica
     * a cargo da procedure.
     * @param mac mac do dosador relacionado aos status
     * @param dosadorStatus Status do dosador
     * @return .
     * @throws Exception . 
     */
    public boolean insereDosadorStatus(long mac, DosadorStatus dosadorStatus) throws Exception {
        String SQL = " call sp_insert_dosador_status_registro(?,?,?,?,?,?,?)";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                ps.setString(1,Long.toHexString(mac));
                ps.setInt(2, dosadorStatus.getRationSensorStatus());
                ps.setInt(3, dosadorStatus.getAntenaStatus());
                ps.setInt(4, dosadorStatus.getMotorStatus());
                ps.setInt(5, dosadorStatus.getDosadorStatus().getValor());
                ps.setInt(6, dosadorStatus.getMemmoryStatus());
                ps.setInt(7, dosadorStatus.getNetworkStatus());
                rs = ps.executeQuery();
                return true;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }

    /**
     * Insere a notificação de brinco desconhecido
     * @param animal Carrega os dados da TAG do brinco e da data da ocorrencia
     * @return .
     * @throws Exception .
     */
    public boolean insereNotificacaoTagDesconhecida(Animal animal) throws Exception {
        String SQL = " call sp_insert_notificacao_tag_desconhecida(?) ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                
                Date utilDate = animal.getDate().getTime();			
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateForMySql = sdf.format(utilDate);
                ps.setString(1, String.format("{\"mac\":\"%s\",\"tag_brinco\":\"%s\",\"date\":\"%s\"}",Long.toHexString(animal.getUltimoDosador()),Long.toString(animal.getRFID()),dateForMySql));
                rs = ps.executeQuery();
                return true;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }

    /**
     * Insere a notificação de atualização de receitas.
     * @param status O status da requisiçao
     * @return .
     * @throws Exception . 
     */
    public boolean insereNotificacaoReceitasUpdate(String status) throws Exception {
        String SQL = " call sp_insert_notificacao_receita_update(?) ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                ps.setString(1, String.format("{\"status\":\"%s\"}",status));
                rs = ps.executeQuery();
                return true;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }
    
    /**
     * Insere a notificação da atualização dos animais
     * @param status O status da requisiçao
     * @return .
     * @throws Exception .
     */
    public boolean insereNotificacaoAnimaisUpdate(String status) throws Exception {
        String SQL = " call sp_insert_notificacao_animais_update(?) ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                ps.setString(1, String.format("{\"status\":\"%s\"}",status));
                rs = ps.executeQuery();
                return true;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }

    /**
     * Insere a notificação da exclusão de dosador
     * @param MAC Endereço do dosador
     * @param status O status da requisiçao
     * @return .
     * @throws Exception  .
     */
    public boolean insereNotificacaoExclusaoDosador(String MAC, String status) throws Exception {
        String SQL = " call sp_insert_notificacao_exclusao_dosador(?,?) ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                ps.setString(1, MAC);
                ps.setString(2, String.format("{\"mac\":\"%s\",\"status\":\"%s\"}", MAC,status));
                rs = ps.executeQuery();
                return true;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }

    /**
     * Busca os parametros de inicialização do agente
     * @param agenteID O ID do Agente
     * @return Retorna os parametros
     * @throws Exception .
     */
    public ParamsAgente buscaParametrosAgente(int agenteID) throws Exception {
        ParamsAgente params = new ParamsAgente();
        String SQL = " call sp_select_parametros_agente(?) ";
        try {
            ps = (PreparedStatement) conn.prepareStatement(SQL);
            ps.setInt(1, agenteID);
            rs = ps.executeQuery();
            if(rs.next()) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
                String date = rs.getTime("t_varredura_notificacao").toString();
                Date t= sdf.parse(date);
                params.setT_Notficacoes((int)t.getTime()/1000);
                date=rs.getTimestamp("t_varredura_rede").toString();
                t= sdf.parse(date);
                params.setT_Varredura((int)t.getTime()/1000);
            }else{
                params.setT_Notficacoes(60);
                params.setT_Varredura(60);
            }
            return params;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }       
    }
    
    /**
     * Insere o status do agente
     * @param agenteID ID do Agente
     * @throws Exception .
     */
    public void insereAgenteStatus(int agenteID) throws Exception {
        String SQL = " call sp_select_parametros_agente(?) ";
        try{
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                ps.setInt(1, agenteID);
                rs = ps.executeQuery();
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }      
    }
    
    /**
     * Busca os parametros de tempo dos dosadores alterados
     * @return Retorna a lista dos dosadores alterados e seus respectivos tempos
     * @throws Exception .
     */
    public ArrayList<DosadorTime> buscaDosadorTimeUpdate() throws Exception {
        ArrayList<DosadorTime> dTime = new ArrayList<>();
        String SQL = " call sp_select_parametros_dosador_update() ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                rs = ps.executeQuery();
                while(rs.next()) {
                        DosadorTime dt=new DosadorTime();
                        dt.setDosador(Long.parseLong(rs.getString("pMac"), 16));
                        dt.setID(rs.getInt("pid"));
                        dt.setFeedBegin(rs.getShort("pTi"));
                        dt.setFeed(rs.getShort("pTd"));
                        dt.setMotorFeed(rs.getShort("pTa"));
                        dt.setMotorStop(rs.getShort("pTp"));
                        dt.setMotorReverse(rs.getShort("pTr"));
                        dt.setAnimalOut(rs.getShort("pTs"));
                        dTime.add(dt);
                }
                return dTime;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }       
    }

    /**
     * Busca o numero de animais cadastrados
     * @return retorna o número de animais
     * @throws SQLException .
     * @throws Exception .
     */
    public int buscaNAnimais() throws SQLException, Exception {
        String SQL = " call sp_busca_n_animais() ";
        try {
            ps = (PreparedStatement) conn.prepareStatement(SQL);
            rs = ps.executeQuery();
            if(rs.next()){
                return rs.getInt("nanimais");
            }
            return 0;
        }catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }       
    }
        /**
     * Atualiza no BD os dosadores com o BD local sincronizado
     * @param d_sinc Dosadores sincronizados
     * @param d_n_sinc Dosadores não sincronizados
     * @param tipo Tipo de update do dosador, dieta, params, animal
     * @throws Exception Excessão do Sql
     */
    public void updateDosadoresSinc(String d_sinc, String d_n_sinc,String tipo) throws Exception {
        String extesao="";
        if(tipo.equals("params"))
            extesao=",ag_update=0";
        //Sincronizado
        String SQL1 = "update tb_dosadores set sincronizado=1"+extesao+" where id in ("+d_sinc+");";
        //Não sincronizado
        String SQL2 = "update tb_dosadores set sincronizado=0"+extesao+" where id in ("+d_n_sinc+");";
        try {
            //Atualiza os dosadores sincronizados
            if(!d_sinc.equals("")){
                ps = (PreparedStatement) conn.prepareStatement(SQL1);
                ps.executeUpdate();
            }
            //Atualiza os dosadores não sincronizado
            if(!d_n_sinc.equals("")){
                ps = (PreparedStatement) conn.prepareStatement(SQL2);
                ps.executeUpdate();
            }
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }       
    }
    /**
     * Limpa a flag ag_update dos animais sincronizados nos dosadores.
     * @param animais   lista das chaves primarias dos animais
     * @throws Exception .
     */
    public void updateAnimaisSinc(String animais) throws Exception {
        //Sincronizado
        String SQL1 = "update tb_animais set ag_update=0 where id in ("+animais+");";
        try {//Atualiza os dosadores sincronizados
            ps = (PreparedStatement) conn.prepareStatement(SQL1);
            ps.executeUpdate();
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }       
    }
    /**
     * Insere a notificação da atualização de parametros
     * @param status O status da requisiçao
     * @return .
     * @throws Exception  .
     */
    public boolean insereNotificacaoTimeUpdate(String status) throws Exception {
        String SQL = " call sp_insert_notificacao_time_dosador(?) ";
        try {
                ps = (PreparedStatement) conn.prepareStatement(SQL);
                ps.setString(1, String.format("{\"status\":\"%s\"}",status));
                rs = ps.executeQuery();
                return true;
        } catch (SQLException sqle) {
                throw new Exception(sqle);
        } finally {
                MySqlConfig.closeConnection(conn, ps, rs);
        }
    }
    
}
