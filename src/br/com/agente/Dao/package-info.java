/**
 * Contém as Classes e métodos relacionados ao Banco de dados.<br>
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">jin.ss.ptu@gmail.com</a> e Diogo Patto
 */
package br.com.agente.Dao;