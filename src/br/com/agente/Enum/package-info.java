/**
 * Contém os ENUM para os diversos identificadores de mensagens utilizados nas comunicações.<br>
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">jin.ss.ptu@gmail.com</a>
 */
package br.com.agente.Enum;