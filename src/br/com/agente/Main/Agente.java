/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agente.Main;

import br.com.agente.Bean.Animal;
import br.com.agente.Bean.Dosador;
import br.com.agente.Bean.NetworkMsgBean;
import br.com.agente.Bean.NotificacaoBD;
import br.com.agente.Serial.ParamesSerialBean;
import br.com.agente.Bean.Receipe;
import br.com.agente.Dao.ServiceDao;
import br.com.agente.Bean.DosadorStatus;
import br.com.agente.Bean.DosadorTime;
import br.com.agente.Bean.ParamsAgente;
import br.com.agente.Bean.XbeeMsgBean;
import br.com.agente.Enum.DosadorStatusEnum;
import br.com.agente.Enum.MsgNetworkType;
import br.com.agente.MsgFormat.XbeeMsg;
import br.com.agente.Serial.Conexao;
import br.com.agente.Update.UpdateDosador;
import java.awt.EventQueue;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TooManyListenersException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Classe principal, onde estão as threads de requisisção e Tratamento das notificações.(Maiores detalhes <a href="../../../../docs/doc.pdf">aqui</a>).
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */
public class Agente implements Observer{
    private long BROADCAST_ADDRESS;
    private Conexao conexao=new Conexao();
    private long tVarredura=60,tNotificacoes=60;
    final Timer requestTH=new Timer();
    final NotifyCheck notifyTH=new NotifyCheck();;
    final RequestUpdate requestTask=new RequestUpdate();;
    ArrayList<Dosador> dosadores;
    byte marcador_request,marcador_update,contador_stack_ref;
    ParamsAgente params_agente=new ParamsAgente();
    DosadorStatus dosadorStatus;
    short receipe_checksum;
    long rfid_checksum, timeoutNUDB, t_freeze=60000;
    Sincronismo ctr_req=new Sincronismo();
    Sincronismo sinc=new Sincronismo();
    Sincronismo upd=new Sincronismo();
    int agenteID;
    Sincronismo contador_stack=new Sincronismo();
    JSONObject obj_cfg;
    int update_flag=0;
    /**
     * @throws java.lang.Exception Exeção da conexao serial
     */
    private Agente() throws Exception{
        //Carrega as configurações do 
        JSONParser parser = new JSONParser();
        obj_cfg=(JSONObject) parser.parse(new FileReader("config.json"));
        agenteID=Integer.valueOf((String)obj_cfg.get("id_agente"));
        this.BROADCAST_ADDRESS = 0xFFFF;
        sinc.setVal(3);
        String resposta = "";
        ParamesSerialBean parametros = new ParamesSerialBean();
        parametros.setPort((String) obj_cfg.get("port"));
        parametros.setBaud(115200);
        parametros.setData_bits(8);
        parametros.setParity(0);
        parametros.setStop_bits(1);
        parametros.setTime_out(2000);
        dosadorStatus=new DosadorStatus();
        try {
            resposta = conexao.Conectar(parametros);
        } catch (TooManyListenersException | InterruptedException ex) {
            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(resposta.equals("Conectado")) {
            params_agente=new ServiceDao(obj_cfg).buscaParametrosAgente(agenteID);
            tVarredura=30000;//params_agente.getT_Varredura();
            tNotificacoes=10000;//params_agente.getT_Notficacoes();
            dosadores=new ServiceDao(obj_cfg).buscaDosadoresLista();                   //Busca lista de dosadores para requisiçao
            requestTH.schedule(requestTask, 1,tVarredura);
            notifyTH.main(null);
        }else{
            System.out.println("Sem resposta da porta Serial."); 
            Thread.sleep(2000);
            System.exit(1);
        }
        System.out.println(resposta); 
    }
    /**
     * Método Principal
     * @param args Argumentos de inicialização
     * @throws java.lang.Exception Exeção da conexao serial
     */   
    public static  void main(String[] args) throws Exception {
        Agente ag =new Agente();
        ag.conexao.addObserver(ag);
        EventQueue.invokeLater(() -> {
        });
    }
    /**
    * Observador da serial, qualquer mensagem do xbee recebido pela serial é 
    * notificado ao observador que recebe a mensagem no formato de array de 
    * char sendo que esse array em dados brutos deve ser tratado na classe
    * {@link br.com.agente.MsgFormat.XbeeMsg} O método {@link br.com.agente.MsgFormat.XbeeMsg#format(char[])}
    * recebe o array e retorna o objeto do tipo {@link br.com.agente.Bean.XbeeMsgBean}
    * o qual possui os parametros do pacote de mensagem do padrao xbee API(maiores detalhes ver 
    * <a href="https://www.digi.com/resources/documentation/digidocs/pdfs/90002002.pdf">Datasheet</a>) 
    * e o Bean da classe {@link br.com.agente.Bean.NetworkMsgBean}
    * que corresponde aos parametos da rede dos dosadores, sendo o pacote de dados
    * sendo tratado na rede.<br>
    * Para o caso dos pacotes de rede do tipo
    * {@link br.com.agente.Enum.MsgNetworkType#DOSADOR_STACK_STATUS} que retorna os dados de
    * status do dosador, como ração, falha de mensagens, falha memoria, falha antena
    * esse status é enviado para o banco de dados, refletindo ostatus do dosador.<br>
    * Para o caso dos pacotes de rede do tipo
    * {@link br.com.agente.Enum.MsgNetworkType#DOSADOR_STACK_STATUS}
    * Carreg um evento de alimentação de um animal com o ID de 16bits de identificação
    * ao receber a mensagem o evento é enviado ao banco de dados.
    * @see br.com.agente.Bean.NetworkMsgBean
    * @see br.com.agente.Bean.XbeeMsgBean
    * @see br.com.agente.MsgFormat.XbeeMsg#format(char[])
    * @see br.com.agente.MsgFormat.XbeeMsg
    * @param dados Array de char recebido pela Serial
    * @param o Objeto do observado
    */
    @Override
    public void update(Observable o, Object dados) {
        if(update_flag==1){
           return;
        }
        XbeeMsg xbee=new XbeeMsg();
        char mensagem[]=(char[]) dados;
        mensagem[0]=0x10;
        XbeeMsgBean msg;
        msg = xbee.format(mensagem);
        Animal animal;
        switch(msg.getXbee_msg_type()){
            case RECEIVED_PACKAGE:
                //Trata a recepção de mensagens da rede
                switch(msg.getNetworMsg().getNetwork_msg_type()){
                    case DOSADOR_STACK_STATUS:             //Mensagem de status e códigos de erro
                        contador_stack.setVal((byte) msg.getNetworMsg().getStack_size());
                        dosadorStatus.setRationSensorStatus(msg.getNetworMsg().getDosadorStatus().getRationSensorStatus());
                        dosadorStatus.setMotorStatus(msg.getNetworMsg().getDosadorStatus().getMotorStatus());
                        dosadorStatus.setAntenaStatus(0);
                        dosadorStatus.setMemmoryStatus(0);//msg.getNetworMsg().getMemmoryStatus());
                        dosadorStatus.setNetworkStatus(0);//msg.getNetworMsg().getNetworkStatus());
                        break;
                    case DOSADOR_DATA_UPDATE:               //Mensagens trocadas entre dosadoes (Nada a fazer pelo Agente)
                        return;
                    case DOSADOR_STACK_DATA:               //Um evento de dosagem descarregado da pilha do dosador
                        //Localiza o animal de acordo com o ID_16
                        animal=msg.getNetworMsg().getAnimal();
                        animal.setUltimoDosador(msg.getAddress64());
                        try {
                            new ServiceDao(obj_cfg).insereNovoEventoDosagem(animal);
                        } catch (Exception ex) {
                            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                        }
                            contador_stack.setVal(contador_stack.getVal()-1);
                        break;
                    //Função de notificação de novo dosador na rede
                    case DOSADOR_NEW_DEVICE:
                        //Insere o novo dosador no Banco de dados ainda não configurado
                        try {
                            new ServiceDao(obj_cfg).insereDosador(Long.toHexString(msg.getAddress64()));
                        }catch (Exception ex) {
                            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        try {
                            //Insere um novo evento de novo dosador no banco de dadso
                            new ServiceDao(obj_cfg).insereNotificacaoNovoDosador(Long.toHexString(msg.getAddress64()));
                        } catch (Exception ex) {
                            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    case DOSADOR_NEW_DEVICE_OK:
                        XbeeMsg msg_xbee=new XbeeMsg();
                        NetworkMsgBean msg_send=new NetworkMsgBean();
                        //Verificação dos checksum de receita e rfid
                        //Caso nao sejam iguis houve perda de mensagens
                        msg_send.setNetwork_msg_type(MsgNetworkType.AGENTE_NEW_DEVICE_END);
                        msg_xbee.conexao=conexao;
                        if(rfid_checksum==msg.getNetworMsg().getRfidChecksum()&&receipe_checksum==msg.getNetworMsg().getReceipeChecksum()){
                            System.out.println("Pareamento foi um Sucesso MAC: "+Long.toHexString(msg.getAddress64()));
                            try {
                                    msg_xbee.sendNetworkMsg(msg.getAddress64(), msg_send);
                                    new ServiceDao(obj_cfg).insereNotificacaoNovoDosadorFim(Long.toHexString(msg.getAddress64()), "Sucesso");
                                    dosadores=new ServiceDao(obj_cfg).buscaDosadoresLista();   //Busca lista de dosadores para requisiçao
                            } catch (Exception ex) {
                                Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }else{
                            System.err.println("Pareamento falhou - Perda de dados");
                            try {
                                new ServiceDao(obj_cfg).insereNotificacaoNovoDosadorFim(Long.toHexString(msg.getAddress64()), "Falha Checksum");
                            } catch (Exception ex) {
                                Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        break;
                    case DOSADOR_RFID_NOT_FOUND:
                        //Insere a notificação de brinco desconhecido no banco de dados
                        try {
                            new ServiceDao(obj_cfg).insereNotificacaoTagDesconhecida(msg.getNetworMsg().getAnimal());
                        } catch (Exception ex) {
                            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        contador_stack.setVal(contador_stack.getVal()-1);
                        break;
                } 
                break;
            case TRANSMIT_STATUS:
                timeoutNUDB=System.currentTimeMillis();;//COntrole de timeout nas respostas
                if(ctr_req.getVal()==0){
                    if(msg.getTransmitStatus().getFrameID()==marcador_request)
                        if(msg.getTransmitStatus().getDeliveryStatus()==0){
                            ctr_req.setVal(1);
                        }else{
                            ctr_req.setVal(2);
                        }
                }
                if(upd.getVal()==1){
                    if(msg.getTransmitStatus().getFrameID()==marcador_update)
                        if(msg.getTransmitStatus().getDeliveryStatus()==0){      //Address not found
                            upd.setVal(0);                            
                        }
                        else{
                            upd.setVal(3); 
                        }
                }
                break;
        }        
    }
    /**
     * Requisições periodicas dos dosadores com as mensagens do tipo
     * {@link br.com.agente.Enum.MsgNetworkType#AGENTE_UPDATE_REQUEST}.<br>
     * Faz a varredura da lista de dosadores ativos com a requisição a requisição
     * de um por um e aguarda a resposta com a mensagem do tipo {@link br.com.agente.Enum.MsgNetworkType#DOSADOR_STACK_STATUS}
     * que é recebido no Observer da serial ({@link #update(java.util.Observable, java.lang.Object) }).
     * A quantidade de mensagens armazenada na pilha do dosador é recebida junto e verificado, no recebimento das demais mensagens do tipo
     * {@link br.com.agente.Enum.MsgNetworkType#DOSADOR_STACK_DATA} que possui as informações de alimentação.
     * @see br.com.agente.Enum.MsgNetworkType#DOSADOR_STACK_STATUS
     * @see br.com.agente.Enum.MsgNetworkType#AGENTE_UPDATE_REQUEST
     * @see br.com.agente.Main.Agente#update(java.util.Observable, java.lang.Object) 
     * gegerando os eventos de alimentação e estado dos dosadores  
     */
    private class RequestUpdate extends TimerTask{
        private int runFlag=0;
        public int isRunnig(){
            return runFlag;
        }
        private int scanear() throws Exception{
            XbeeMsg msg_send = new XbeeMsg();
            NetworkMsgBean msg = new NetworkMsgBean();
            msg.setNetwork_msg_type(MsgNetworkType.AGENTE_UPDATE_REQUEST);
            msg_send.conexao = conexao;
            long timeout = 3000;
            for (Dosador d : dosadores) {
                marcador_request = msg_send.sendNetworkMsg(d.getMAC(), msg);
                ctr_req.setVal(0);
                contador_stack.setVal(1);
                contador_stack_ref = 1;
                long t = System.currentTimeMillis();
                switch (sinc.getVal()) {
                    case 0:
                        System.out.println("Thread ReqUp Saiu da varredura");
                        sinc.setVal(5);
                        return 5;
                    case 4:
                        System.out.println("Thread ReqUp Saiu da varredura");
                        sinc.setVal(1);
                        return 1;
                    default:
                        break;
                }
                while ((ctr_req.getVal() == 0) && (System.currentTimeMillis() - t) < timeout) {
                }
                switch (ctr_req.getVal()) {
                    case 0: //Ocorreu o timeout na espera da resposta do coordenador da rede
                        //Nunca deve ocorrer, pode ser sobrecarga na rede
                        //new ServiceDao().insereNetworkStatus(d.getMAC(),"{\"Status\":\"Sem Resposta Rede\"}");
                        break;
                    case 1: //Ocorreu Sucesso na resposta do coordenador da rede, passa para a rotina de recepção
                        //System.out.println("Dosador 0"+contador_stack+" msg");
                        t = System.currentTimeMillis();
                        while ((contador_stack.getVal() > 0) && (System.currentTimeMillis() - t) < timeout) {
                            if (contador_stack.getVal() < contador_stack_ref) {
                                t = System.currentTimeMillis();
                                contador_stack_ref = (byte) contador_stack.getVal();
                            }
                        }
                        if (contador_stack.getVal() > 0) {
                            dosadorStatus.setDosadorStatus(DosadorStatusEnum.DOSADOR_LOST_MSG);
                            new ServiceDao(obj_cfg).insereDosadorStatus(d.getMAC(), dosadorStatus);
                        } else {
                            dosadorStatus.setDosadorStatus(DosadorStatusEnum.DOSADOR_OK);
                            new ServiceDao(obj_cfg).insereDosadorStatus(d.getMAC(), dosadorStatus);
                        }
                        break;
                    case 2:
                        //Dosador sem resposta
                        //System.out.println("Dosador "+d.getID()+" não encontrado.");
                        dosadorStatus.setAntenaStatus(1);
                        dosadorStatus.setMotorStatus(1);
                        dosadorStatus.setRationSensorStatus(1);
                        dosadorStatus.setDosadorStatus(DosadorStatusEnum.DOSADOR_NOT_FOUND);
                        System.out.println("Nao encontrado " + Long.toHexString(d.getMAC()));
                        new ServiceDao(obj_cfg).insereDosadorStatus(d.getMAC(), dosadorStatus);
                        break;
                }
            }
            return 0;
        }
        
        @Override
        public void run() {
            synchronized(this){
                runFlag=1;
                //Busca todos os dosadores no BD
                //Faz a requisição a cada dosador, as respostas são tratadas em Update
                //aguarda um tempo antes de passar para o proximo dosador
                //Decrementa o contador de mensagens recebido pelo dosador
                if(sinc.getVal()==4)
                    sinc.setVal(1);
                switch (sinc.getVal()) {
                    case 1:
                        sinc.setVal(2);
                
                        try {
                            synchronized(notifyTH){
                                notifyTH.wait(60000);
                            }
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                            notify();
                            runFlag=0;
                        }
                        sinc.setVal(3);
                    break;
                }
                System.out.println("Inicio da varredura");
                try {
                    int res=scanear();
                    if (res==5){
                        Thread.sleep(50); 
                        sinc.setVal(1);
                        scanear();
                        System.out.println("Thread ReqUp Parou");
                        sinc.setVal(2);
                        notify();
                        synchronized(notifyTH){
                            notifyTH.wait(60000);
                        }
                    }
                } catch (Exception ex) {
                    Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                }finally{
                    notify();
                }
                notify();
                runFlag=0;
            }
        }
    }
    /**
     * Método que realiza o cadastro de um novo dosador na rede.<br> 
     * Após o BD atualizado é feito
     * a busca no BD o cadastro dos animais e os envia ao dosador, sempre verificando o 
     * sucesso do envio das mensagens e fazendo a soma dos RFID enviados para o teste do checksum 
     * no final do cadastro. Todo o processo fica a cargo do envio dos dados pelo <a>Agente</a> tendo apenas a confirmaçao do envio 
     * pela rede do xbee e nao o processamento do dosador pela mensagem do tipo {@link br.com.agente.Enum.MsgXbeeType#TRANSMIT_STATUS} 
     * (para maiores detalhes ver <a href="https://www.digi.com/resources/documentation/digidocs/pdfs/90002002.pdf">Datasheet</a>).
     * <br>Em seguida são enviadas as receitas e tambem é feito o checksum de cada byte que representa o QR diario
     * @param dosador_address Endereço MAC do dosador a ser cadastrado
     * @see br.com.agente.Enum.MsgXbeeType#TRANSMIT_STATUS
     * @throws Exception  Exceção de Serial e Banco de dados
     * @throws InterruptedException Exceção de Thread.sleep()
     * @return Status do cadastro do dosador, falha ou sucesso
     */  
    private byte novoDosador(long dosador_address) throws InterruptedException, Exception{
        XbeeMsg msg_send=new XbeeMsg();
        NetworkMsgBean msg=new NetworkMsgBean();
        msg_send.conexao=conexao;                       //Expecifica a conexao a ser usada
        //////////////////////////////////////////////////////Envia os animais///////////////////////////////////////////////////////
        ArrayList<Animal> animais =new ServiceDao(obj_cfg).buscaAnimaisLista();
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_ANIMAL_UPDATE);
        rfid_checksum=0;
        Receipe re=new Receipe();
        msg.setReceipe(re);
        upd.setVal(1);
        int k;
        if(animais.size()>0){
            k=animais.get(animais.size()-1).getID()+1;
            int j=0;
            for (int i=0;i<k;i++) {
                if(animais.get(j).getID()==i){
                    rfid_checksum+=animais.get(j).getRFID();
                    msg.setAnimal(animais.get(j));
                    try {
                        marcador_update=msg_send.sendNetworkMsg(dosador_address, msg);
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                        return 1;
                    }
                    j++;
                }else{
                    try {
                        msg.setAnimal(new Animal());
                        msg.getAnimal().setRFID(0);
                        msg.getAnimal().setID((byte)i);
                        marcador_update=msg_send.sendNetworkMsg(dosador_address, msg);
                        Thread.sleep(50);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                        return 1;
                    }
                }
                    long to=System.currentTimeMillis();
                    while((upd.getVal()==1)&&(System.currentTimeMillis()-to)<1000){
                    }
                    if(upd.getVal()==3){
                        return 2;
                    }
                    if(upd.getVal()==1){
                        return 2;
                    }
                    upd.setVal(1);
                
            }
        }
        
        /////////////////////////////////////////////////////////////Envia as Receitas/////////////////////////////////////////////////////////////
        ArrayList<Receipe> receitas =new ServiceDao(obj_cfg).buscaReceitasLista();
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE);
        receipe_checksum=0;
        receitas.stream().map((r) -> {
            for(int i=0; i<120;i++)
                receipe_checksum+=r.getQRdia()[i];
            return r;
        }).map((r) -> {
            msg.setReceipe(r);
            return r;
        }).forEachOrdered((_item) -> {
            msg_send.sendNetworkReceipe(dosador_address, msg);
        });
        
        ////////////////////////////////////////////////////Atualiza a hora e Mudança de dia///////////////////////////////////////////////////////
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_DATE_HOUR_UP);
        short new_day;
        new_day=11;
        new_day=(short) (new_day<<8);
        new_day+=30;
        msg.setNewDay(new_day);
        msg_send.sendNetworkMsg(dosador_address, msg);

        /////////////////////////////////////////////////////////////Atualiza os Tempos do Dosador//////////////////////////////////////////////////
        DosadorTime t =new ServiceDao(obj_cfg).buscaDosadorTempos(Long.toHexString(dosador_address));
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_PARAMS_UP);
        msg.setDosadorTime(t);
        try {
            msg_send.sendNetworkMsg(dosador_address, msg);
        } catch (InterruptedException ex) {
            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
            return 2;
        }

        ////////////////////////////////////////Finaliza a configuração e coloca o dosador em operação/////////////////////////////////////////////        
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_NEW_DEVICE_UP);
        if(animais.size()>2000)
            msg.setNRFID((short)2000);
        else if(animais.size()>0)
            msg.setNRFID((short) (animais.get(animais.size()-1).getID()+1));
        else
            msg.setNRFID((short)0);
        if(receitas.size()>20)
            msg.setNReceipe((short)20);
        else if(animais.size()>0)
            msg.setNReceipe(receitas.get(receitas.size()-1).getID());
        else
            msg.setNReceipe((short)0);
        try {
            msg_send.sendNetworkMsg(dosador_address, msg);
        } catch (InterruptedException ex) {
            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
            return 2;
        }
        return 0;
    }
    /**
     * Realiza a requisição ao BD em um tempo predeterminado verificando se existe
     * alguma notificação para o agente, assim que recebido essas  notificações 
     * são tratadas. Os parametros necessários para as notificações são passados no formato JSON
     * sendo que cada tipo de notificação possui seus parametros definidos diferentemente.
     */
    private class NotifyCheck extends Thread{
        public void main(String[] args){
            final ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
            ses.scheduleWithFixedDelay(() -> {
                try {
                    //Notifica que o agente está ativo
                    new ServiceDao(obj_cfg).insereAgenteStatus(agenteID);
                } catch (Exception ex) {
                    Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    NotificacaoBD notficacao = new ServiceDao(obj_cfg).buscaNotificacaoAgente();
                    if(notficacao.getCode()!=null){                 //Verifica se ocorreu há notificação
                        System.out.println("Notificação "+notficacao.getCode());
                        JSONObject obj;
                        JSONParser parser = new JSONParser();
                        String resposta="";
                        switch(notficacao.getCode()){
                            case NOPEN:
                                try {
                                    conexao.Enviar(new byte[]{0x7E,0x00,0x05,0x08,0x01,0x43,0x42,0x02,0x6F});
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                break;
                            case NDCONF:
                                synchronized(requestTask){
                                    sinc.setVal(0);
                                    if(requestTask.isRunnig()==1){
                                        requestTask.wait(60000);
                                    }
                                    sinc.setVal(8);
                                    System.err.println("Inicio Update");
                                    RequestUpdate reqTemp=new RequestUpdate();
                                    synchronized(reqTemp){
                                        Thread.sleep(10);
                                        reqTemp.run();
                                        reqTemp.wait(30000);
                                    }
                                    obj=(JSONObject) parser.parse(notficacao.getValor());
                                    byte ans=novoDosador(Long.parseLong((String) obj.get("mac"), 16));
                                    if(ans==3){
                                        new ServiceDao(obj_cfg).insereNotificacaoNovoDosadorFim((String) obj.get("mac"),"Falha atualização BD");
                                        System.err.println("Falha atualização BD");
                                    }
                                    if(ans==2){
                                        new ServiceDao(obj_cfg).insereNotificacaoNovoDosadorFim((String) obj.get("mac"),"Dosador não encontrado");
                                        System.err.println("Dosador não encontrado.");
                                    }
                                    sinc.setVal(3);
                                    synchronized(this){
                                        notify();
                                    }
                                }
                                break;
                            case DRSTR:
                                update_flag=1;
                                XbeeMsg msg_xbee=new XbeeMsg();
                                NetworkMsgBean msg_send=new NetworkMsgBean();
                                msg_send.setNetwork_msg_type(MsgNetworkType.AGENTE_DOSADOR_RESET);
                                msg_xbee.conexao=conexao;
                                obj=(JSONObject) parser.parse(notficacao.getValor());
                                long dosador=Long.parseLong((String) obj.get("mac"), 16);
                                try {
                                    msg_xbee.sendNetworkMsg(dosador, msg_send);
                                    new ServiceDao(obj_cfg).insereNotificacaoExclusaoDosador(Long.toHexString(dosador),"Sucesso");
                                    System.out.println("Dosador "+Long.toHexString(dosador)+" Excluido com Sucesso");
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                break;
                            case AUPREQ:  //Atualizar RFID
                                update_flag=1;
                                freezeNetwork();
                                ArrayList<Animal> animais= new ServiceDao(obj_cfg).buscaAnimaisListaUpdate();
                                int Nanimais=new ServiceDao(obj_cfg).buscaNAnimais();
                                resposta=new UpdateDosador(dosadores, conexao,obj_cfg).uAnimais(animais,Nanimais);
                                new ServiceDao(obj_cfg).insereNotificacaoAnimaisUpdate(resposta);
                                //Carrega os dosadores ativos
                                dosadores=new ServiceDao(obj_cfg).buscaDosadoresLista(); 
                                break;
                            case RUPREQ:   //Atualizar Receitas
                                update_flag=1;
                                freezeNetwork();
                                ArrayList<Receipe> receipes = new ServiceDao(obj_cfg).buscaReceitasUpdate();
                                resposta = new UpdateDosador(dosadores,conexao,obj_cfg).uReceipe(receipes);
                                new ServiceDao(obj_cfg).insereNotificacaoReceitasUpdate(resposta);
                                //Carrega os dosadores ativos
                                dosadores=new ServiceDao(obj_cfg).buscaDosadoresLista(); 
                                break;
                            case TUPREQ:   //Atualiza os Parametros de tempo
                                update_flag=1;
                                freezeNetwork();
                                ArrayList<DosadorTime> dTime = new ServiceDao(obj_cfg).buscaDosadorTimeUpdate();
                                resposta = new UpdateDosador(dosadores,conexao,obj_cfg).uDosadorTime(dTime);
                                new ServiceDao(obj_cfg).insereNotificacaoTimeUpdate(resposta);
                                //Carrega os dosadores ativos
                                dosadores=new ServiceDao(obj_cfg).buscaDosadoresLista(); 
                                break;                        
                            case NSTOP:   //Para a rede
                                break;
                        }
                    }else
                        System.out.println("Notificação Vazio");
                } catch (Exception ex) {
                    update_flag=0;
                    Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
                }
                update_flag=0;
            }, 0,tNotificacoes, TimeUnit.MILLISECONDS);
        }
    }
    /**
     * Para as requisiçoes e trocas de mensagens na rede para realizar alguma alteração
     * que não necessite da atualização do banco de dados e caso necessite de atualização
     * @throws InterruptedException Excessão no uso de Thread.sleep()
     * @return char
     */
    private char freezeNetwork() throws InterruptedException{
        XbeeMsg msg_send=new XbeeMsg();
        NetworkMsgBean msg=new NetworkMsgBean();
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_NETWORK_FREEZE);          //Mensagem para paralizar a rede
        msg_send.conexao=conexao;                                               //Expecifica a conexao a ser usada
        msg.setFreezeNetworkTime((short)60);                                    //Tempo de paralização da rede     
        msg_send.sendNetworkMsg(BROADCAST_ADDRESS, msg);                        //Paraliza a rede pelo tempo especificado
        long t=System.currentTimeMillis();                                      
        long timeout=10000;
        sinc.setVal(4);                                                         //habilita a flag de sincronismo
        while((sinc.getVal()==4)&&(System.currentTimeMillis()-t)<timeout){
        }
        if(sinc.getVal()==1){
            t=System.currentTimeMillis();
            while((sinc.getVal()==1)&&(System.currentTimeMillis()-t)<timeoutNUDB){
            }
            if(sinc.getVal()==2){
                Thread.sleep(500);                                              //Aguarda um tempo para a troca das ultimas mensagens na rede
                return 1;
            }
        }
        return 0;
    }

    /**
     *Classe que implementa syncronized nos métodos de acesso a variável valor
     * para que esta seja acessada pelas diversas threads sem que haja o conflito 
     * de aceeso, pois caso uma Thread mude o valor de val a outra tambem acessa a alteração.
     */
    private class Sincronismo{
        int val=1;
        /**
         * Pega o valor de val no contexto sincronizado
         * @return the val
         */
       public synchronized int getVal() {
           return val;
       }

       /**
        * Atribui valor a val no contexto sincronizado
        * @param val the val to set
        */
       public synchronized void setVal(int val) {
           this.val = val;
       }
    }
}
