/**
 * Contém a classe principal do Agente que implementas as Threads de varredura da rede de dosadoes e tratamento das notificações.<br>
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">jin.ss.ptu@gmail.com</a>
 */
package br.com.agente.Main;