/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agente.MsgFormat;

import br.com.agente.Bean.Animal;
import br.com.agente.Bean.NetworkMsgBean;
import br.com.agente.Bean.TransmitStatusMsg;
import br.com.agente.Bean.XbeeMsgBean;
import br.com.agente.Enum.MsgNetworkType;
import br.com.agente.Enum.MsgXbeeType;
import br.com.agente.Main.Agente;
import br.com.agente.Serial.Conexao;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Classe para formatação de mensagens do Xbee e envio de mensagens na rede dos dosadores
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */

public class XbeeMsg implements Observer{
    public Conexao conexao;
    public char data[];
    private byte control=0;
    private int tx_status;
    private int id_msg=0;
    /**
     * Formata a mensagem do xbee e dependendo do pacote recebido ({@link br.com.agente.Enum.MsgXbeeType#RECEIVED_PACKAGE}), formata a mensagem da rede
     * chamando o método {@link br.com.agente.MsgFormat.NetworkMsg#format(char[])};
     * @return retorna o objeto com os dados da mensagem do xbee e da rede.
     * @see br.com.agente.MsgFormat.NetworkMsg#format(char[]) 
     * @param data array recebido pela Serial do xbee
     */
    public XbeeMsgBean format(char data[]){
        XbeeMsgBean msg=new XbeeMsgBean();
        msg.setXbee_msg_type(MsgXbeeType.findType((int)data[1])); 
        short add16;
        switch(msg.getXbee_msg_type()){
            case RECEIVED_PACKAGE:
                long address=0;
                for(int i=2;i<10;i++){
                    address=address<<8;
                    address=address|(long)data[i];
                }
                msg.setAddress64(address);
                add16=(short) (data[10]*256);
                add16+=(short) data[11];
                msg.setAddress16(add16);
                NetworkMsg network_msg=new NetworkMsg();
                msg.setNetworMsg(network_msg.format(data));
                NetworkMsgBean nm=msg.getNetworMsg();
                Animal a_temp= nm.getAnimal(); //msg.getNetworMsg().getAnimal();
                a_temp.setUltimoDosador(address);
                nm.setAnimal(a_temp);
                msg.setNetworMsg(nm);
                break;
            case TRANSMIT_STATUS:
                add16=(short) (data[3]*256);
                add16+=(short) data[4];
                msg.setAddress16(add16);
                TransmitStatusMsg transmitStatus=new TransmitStatusMsg();
                transmitStatus.setTransmitRetryCount((byte) data[5]);
                transmitStatus.setDeliveryStatus((byte) data[6]);
                transmitStatus.setDiscoveryStatus((byte) data[7]);
                transmitStatus.setFrameID((byte) data[2]);
                msg.setTransmitStatus(transmitStatus);
                break;
        }
        return msg;
    }
    /**
     * Formata a data-hora para o formato utilizado na rede
     * @param date Data a ser formatada
     * @return Retorna valor long com o valor da data em 32bits unsigned
     */
    public long dataFormat(Calendar date){
        long date_time=0;
        date_time|=((date.get(Calendar.DAY_OF_MONTH))&0b11111);
        date_time=date_time<<4;
        date_time|=((date.get(Calendar.MONTH)+1)&0b1111);
        date_time=date_time<<6;
        date_time|=(date.get(Calendar.YEAR)-2000)&0b111111;
        date_time=date_time<<5;
        date_time|=(date.get(Calendar.HOUR_OF_DAY)&0b11111);
        date_time=date_time<<6;
        date_time|=(date.get(Calendar.MINUTE) &0b111111);
        date_time=date_time<<6;
        date_time|=(date.get(Calendar.SECOND)&0b111111);
        return date_time;  
    }
    
    /**
     * Envia uma mensagem para o destinatario (MAC addres) na rede, sendo a mensagem especificada no Bean
     * {@link br.com.agente.Bean.NetworkMsgBean};
     * @param address Endereço do dosador de destino
     * @param msg Mensagem a ser enviada
     * @return Retorna o identificador da mensagem para contorle do transmit_status
     * @throws InterruptedException .
     */
    public byte sendNetworkMsg(long address, NetworkMsgBean msg) throws InterruptedException{
        byte char_msg[];
        byte checksum;
        int start_msg=17;
        switch(msg.getNetwork_msg_type()){
            case AGENTE_UPDATE_REQUEST:
                char_msg=new byte[19];
                char_msg[2]=15;//Tamanho
                break;
            case AGENTE_DEBUG_COMMAND:
                char_msg=new byte[19];
                char_msg[2]=15;//Tamanho
                break;
            case AGENTE_ANIMAL_UPDATE:
                char_msg=new byte[32];
                char_msg[2]=28;//Tamanho
                short id=msg.getAnimal().getID();
                char_msg[start_msg+1]=msg.getAnimal().getDia();
                for(int i=0;i<2;i++){
                    char_msg[start_msg+2+i]=(byte)id;
                    id=(short) (id>>8);
                }
                long rfid=msg.getAnimal().getRFID();
                for(int i=0;i<8;i++){
                    char_msg[start_msg+4+i]=(byte)rfid;
                    rfid=rfid>>8;
                }
                char_msg[29]=msg.getReceipe().getID();
                char_msg[30]=msg.getAnimal().getQR();
                break;
            case AGENTE_RECEIPE_UPDATE_A:
                char_msg=new byte[50];
                char_msg[2]=46;//Tamanho
                char_msg[start_msg+1]=(byte)msg.getReceipe().getID();
                for(int i=0;i<30;i++){
                    char_msg[start_msg+2+i]=(byte) msg.getReceipe().getQRdia()[i];
                }
                break;
            case AGENTE_RECEIPE_UPDATE_B:
                char_msg=new byte[50];
                char_msg[2]=46;//Tamanho
                char_msg[start_msg+1]=(byte)msg.getReceipe().getID();
                for(int i=0;i<30;i++){
                    char_msg[start_msg+2+i]=(byte) msg.getReceipe().getQRdia()[i+30];
                }
                break;
            case AGENTE_RECEIPE_UPDATE_C:
                char_msg=new byte[50];
                char_msg[2]=46;//Tamanho
                char_msg[start_msg+1]=(byte)msg.getReceipe().getID();
                for(int i=0;i<30;i++){
                    char_msg[start_msg+2+i]=(byte) msg.getReceipe().getQRdia()[i+60];
                }
                break;            
            case AGENTE_RECEIPE_UPDATE_D:
                char_msg=new byte[50];
                char_msg[2]=46;//Tamanho
                char_msg[start_msg+1]=(byte)msg.getReceipe().getID();
                for(int i=0;i<30;i++){
                    char_msg[start_msg+2+i]=(byte) msg.getReceipe().getQRdia()[i+90];
                }
                break;
            case AGENTE_NEW_DEVICE_UP:
                char_msg=new byte[23];
                char_msg[2]=19;//Tamanho
                char_msg[start_msg+1]=(byte)msg.getNReceipe();
                short val=msg.getNRFID();
                //RFID
                for(int i=0;i<2;i++){
                    char_msg[start_msg+2+i]=(byte)val;
                    val=(short) (val>>8);
                }
                break;
            case AGENTE_PARAMS_UP:
                char_msg=new byte[36];
                char_msg[2]=32;//Tamanho
                for(int i=0;i<2;i++){
                    char_msg[start_msg+2+i]=(byte)msg.getDosadorTime().getMotorFeed();
                    msg.getDosadorTime().setMotorFeed((short) (msg.getDosadorTime().getMotorFeed()>>8));
                    char_msg[start_msg+4+i]=(byte)msg.getDosadorTime().getMotorReverse();
                    msg.getDosadorTime().setMotorReverse((short) (msg.getDosadorTime().getMotorReverse()>>8));
                    char_msg[start_msg+6+i]=(byte)msg.getDosadorTime().getMotorStop();
                    msg.getDosadorTime().setMotorStop((short) (msg.getDosadorTime().getMotorStop()>>8));
                    char_msg[start_msg+8+i]=(byte)msg.getDosadorTime().getFeedBegin();
                    msg.getDosadorTime().setFeedBegin((short) (msg.getDosadorTime().getFeedBegin()>>8));
                    char_msg[start_msg+10+i]=(byte)msg.getDosadorTime().getFeed();
                    msg.getDosadorTime().setFeed((short) (msg.getDosadorTime().getFeed()>>8));
                    char_msg[start_msg+12+i]=(byte)msg.getDosadorTime().getAnimalOut();
                    msg.getDosadorTime().setAnimalOut((short) (msg.getDosadorTime().getAnimalOut()>>8));                     
                }
                break;
            case AGENTE_DATE_HOUR_UP:
                ;
                char_msg=new byte[50];
                char_msg[2]=46;//Tamanho
                //Data-Hora
                Date dn = new Date();
                Calendar caln = new GregorianCalendar();
                caln.setTime(dn);
                long n_time=dataFormat(caln);
                for(int i=0;i<4;i++){
                    char_msg[start_msg+4+i]=(byte)n_time;
                    n_time=(long) (n_time>>8);
                }
                for(int i=0;i<2;i++){
                    char_msg[start_msg+2+i]=(byte)msg.getNewDay();
                    msg.setNewDay((short) (msg.getNewDay()>>8));
                }
                break;
            case AGENTE_DOSADOR_RESET:
                char_msg=new byte[21];
                char_msg[2]=17;//Tamanho
                char_msg[start_msg+2]='R';//Caractere de Reset
                break;
            case AGENTE_NEW_DEVICE_END:
                char_msg=new byte[19];
                char_msg[2]=15;//Tamanho
                break;
            default:
                return 0;
        }
        char_msg[0]=0x7e;
        char_msg[1]=0;
        char_msg[3]=(byte) MsgXbeeType.TRANSMIT_REQUEST.getValor();
        if(control==0)
            control=1;
        char_msg[4]=control++;
        for(int i=7;i>=0;i--){
            char_msg[5+i]=(byte)address;
            address=address>>8;
        }
        char_msg[13]=(byte) 0xFF;
        char_msg[14]=(byte) 0xFe;
        char_msg[15]=0x00;      //BROADCAST RADIUS  
        char_msg[16]=0x01;      //OPTION
        //Inicio dos dados
        char_msg[17]=(byte) msg.getNetwork_msg_type().getValor();
        checksum=0;
        for(int i=0; i<=char_msg[2];i++){
            checksum+=char_msg[i+3];
        }
        checksum=(byte) (0xFF-checksum);
        char_msg[char_msg.length-1]=checksum;
        conexao.Enviar(char_msg);
        return char_msg[4];
    }
    /**
     * Envia a mensagem e aguarda a resposta do Transmit status, confirmando a entrega ao destinatário.
     * @param dosador Endereço do dosador de destino
     * @param msg Mensagem a ser enviada
     * @return  Retorna o delivery_status da resposta da transmissão
     * @throws java.lang.InterruptedException .
     */
    public int sendNetworkMsgWait(long dosador, NetworkMsgBean msg) throws InterruptedException {
        //System.err.println("  " + Long.toHexString(dosador)+"  ");
        id_msg=sendNetworkMsg(dosador, msg);
        long t=System.currentTimeMillis();
        setTx_status((int)0xff);
        while((getTx_status()==0xFF)&&(System.currentTimeMillis()-t)<1500){
        }
        return getTx_status();
    }
    /**
     * Envia uma receita na rede, devido a receita ser uma quantidade maior de dados ela foi diidida em 4 pacotes,
     * visando a limitação de memória de buffer para as mensagens nos dosadores.
     * @param address Endereço do dosador de destino
     * @param msg Mensagem a ser enviada
     */
    public void sendNetworkReceipe(long address, NetworkMsgBean msg) {
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE_A);
        try {
            sendNetworkMsg(address, msg);
            msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE_B);
            Thread.sleep(10);
            sendNetworkMsg(address, msg);
            msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE_C);
            Thread.sleep(10);
            sendNetworkMsg(address, msg);
            msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE_D); 
            Thread.sleep(10);
            sendNetworkMsg(address, msg);
        } catch (InterruptedException ex) {
            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Observador da serial para verificação do transmit status
     * @param o Retorna o objeto observado
     * @param dados Objeto de dados do observado
     */
    @Override
    public void update(Observable o, Object dados) {
        XbeeMsg xbee=new XbeeMsg();
        char mensagem[]=(char[]) dados;
        XbeeMsgBean msg;
        msg = xbee.format(mensagem);
        if(msg.getXbee_msg_type()==MsgXbeeType.TRANSMIT_STATUS){
            if(id_msg==msg.getTransmitStatus().getFrameID()){
                setTx_status(msg.getTransmitStatus().getDeliveryStatus());
                //System.err.println(msg.getTransmitStatus().getDeliveryStatus());
            }
        }
    }
    /**
     * Envia as partes da receita aguarda a resposta do Transmit status<br>
     * O teste é feito apenas na primeira parte da receita
     * @param address Endereço do dosador de destino
     * @param msg Mensagem a ser enviada
     * @return Retorna o delivery_status da resposta da transmissão
     */
    public int sendNetworkReceipeWait(long address, NetworkMsgBean msg) {

        try {
            msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE_A);
            if(sendNetworkMsgWait(address, msg)!=0)
                return getTx_status();
            msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE_B);
            Thread.sleep(10);
            if(sendNetworkMsgWait(address, msg)!=0)
                return getTx_status();
            msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE_C);
            Thread.sleep(10);
            if(sendNetworkMsgWait(address, msg)!=0)
                return getTx_status();
            msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE_D); 
            Thread.sleep(10);
            if(sendNetworkMsgWait(address, msg)!=0)
                return getTx_status();
        } catch (InterruptedException ex) {
            Logger.getLogger(Agente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    /**
      * Pega o valor de tx_status no contexto sincronizado
      * @return the tx_status
      */
    public synchronized int getTx_status() {
        return tx_status;
    }
    /**
     * Atribui valor a tx_status no contexto sincronizado
     * @param tx_status the val to set
     */
    public synchronized void setTx_status(int tx_status) {
        this.tx_status = tx_status;
}
}
