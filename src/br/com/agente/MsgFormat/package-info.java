/**
 * Contém as classes de formatação das mensagemns do formato API do xbee e as mensagens da 
 * rede de dosadores.<br>
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">jin.ss.ptu@gmail.com</a>
 */
package br.com.agente.MsgFormat;