/**
 * Contém as classe que comandam a conexão serial utilizada no xbee.<br>
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">jin.ss.ptu@gmail.com</a>
 */
package br.com.agente.Serial;