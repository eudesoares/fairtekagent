/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.agente.Update;

import br.com.agente.Bean.Animal;
import br.com.agente.Bean.Dosador;
import br.com.agente.Bean.DosadorTime;
import br.com.agente.Bean.NetworkMsgBean;
import br.com.agente.Bean.Receipe;
import br.com.agente.Dao.ServiceDao;
import br.com.agente.Enum.MsgNetworkType;
import br.com.agente.MsgFormat.XbeeMsg;
import br.com.agente.Serial.Conexao;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 * Classe com os métodos de update dos dosadores, referente as receitas e animais e  parametros de dosadores
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */
public class UpdateDosador {
    Conexao conexao;
    ArrayList<Dosador> dosadores;
    JSONObject con_str;
    
    /**
     * Método construtor da classe
     * @param dosadores Dosadores a serem atualizados 
     * @param conexao Conecção serial a do xbee
     * @param obj Conexão Sql
     */
    public UpdateDosador(ArrayList<Dosador> dosadores, Conexao conexao, JSONObject obj) {
        this.dosadores=dosadores;
        this.conexao=conexao;
        con_str=obj;
    }
    
    /**
     * Atualiza as receitas modificadas de acordo com o retorno do BD que verifica a
     * flag ag_update.
     * @param receitas Receitas a serem atualizadas
     * @return Reotorna o status da atualização
     * @throws java.lang.InterruptedException .
     */
    public String uReceipe(ArrayList<Receipe> receitas) throws InterruptedException{
        String status;
        String d_sinc="",d_n_sinc="";
        int falhas=0;
        XbeeMsg msg_send=new XbeeMsg();
        NetworkMsgBean msg=new NetworkMsgBean();
        msg_send.conexao=conexao;                                       //Expecifica a conexao a ser usada
        conexao.addObserver(msg_send);
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_RECEIPE_UPDATE);
        if(!dosadores.isEmpty()){
            if(!receitas.isEmpty()){
                OUT:
                for (Dosador d : dosadores) {
                    for (Receipe r : receitas) {
                        msg.setReceipe(r);
                            //Envia a mensagem na rede e aguarda a susa resposta de entrega
                            if(msg_send.sendNetworkReceipeWait(d.getMAC(), msg)!=0){
                                if(!d_n_sinc.equals(""))
                                    d_n_sinc+=",";
                                d_n_sinc+=d.getID();
                                falhas++;
                                break OUT;
                            }
                            if(!d_sinc.equals(""))
                                d_sinc+=",";
                            d_sinc+=d.getID();
                    }
                }
            }else{
                return "Nenhuma receita modificada";
            }
        }else{
            return "Nenhum Dosador";
        }
        if(falhas==0)
            status = "Sucesso";
        else if(falhas==dosadores.size())
            status = "Falha - Nenhum Dosador Atualizado.";
        else
           status = "Alerta - Nem Todos dosadores Atualizados.";
        try {
            new ServiceDao(con_str).updateDosadoresSinc(d_sinc, d_n_sinc,"");
        } catch (Exception ex) {
            Logger.getLogger(UpdateDosador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }
    
    /**
     * Atualiza as receitas modificadas de acordo com o retorno do BD que verifica a
     * flag ag_update.
     * @param animais Animais a serem atualizados
     * @param max numero do maior ID 16 dos animais cadastrados
     * @return Retorna o status da atualização
     * @throws InterruptedException .
     */
    public String uAnimais(ArrayList<Animal> animais, int max) throws InterruptedException{
        String status;
        String d_sinc="",d_n_sinc="",animais_ag_up="";
        XbeeMsg msg_send=new XbeeMsg();
        NetworkMsgBean msg=new NetworkMsgBean();
        msg_send.conexao=conexao;                                       //Expecifica a conexao a ser usada
        conexao.addObserver(msg_send);
        int falhas=0;
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_ANIMAL_UPDATE);
        if(!dosadores.isEmpty()){
            if(!animais.isEmpty()){
                for (Dosador d : dosadores) {
                    OUT:
                    if(animais.size()>0){
                        for (Animal a: animais) {
                            if(a.getExcluido()==1){
                                a.setRFID(0);
                                a.setExcluido((byte)0);
                            }
                            msg.setAnimal(a);
                            //Envia a mensagem na rede e aguarda a susa resposta de entrega
                            if(msg_send.sendNetworkMsgWait(d.getMAC(), msg)!=0){
                                if(!d_n_sinc.equals(""))
                                    d_n_sinc+=",";
                                d_n_sinc+=d.getID();
                                falhas++;
                                break OUT;
                            }
                            Thread.sleep(5);
                        }
                        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_NEW_DEVICE_UP);
                        msg.setNRFID((short)(max+1));
                        msg.setNReceipe((short)10);
                        msg_send.sendNetworkMsg(d.getMAC(), msg);
                        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_ANIMAL_UPDATE);
                        if(!d_sinc.equals(""))
                            d_sinc+=",";
                        d_sinc+=d.getID();
                        
                    }
                }
            }else{
                conexao.deleteObserver(msg_send);
                return "Nenhuma animal modificado.";
            }
        }else{
           conexao.deleteObserver(msg_send);
           return "Nenhum Dosador.";
        }
        conexao.deleteObserver(msg_send);
        if(falhas==0)
           status = "Sucesso";
        else if(falhas==dosadores.size())
           status = "Falha - Nenhum Dosador Atualizado.";
        else
           status = "Alerta - Nem Todos dosadores Atualizados.";
        try {
            //Limpa a flag ag_update dos animais
            if(animais.size()>0){
                for(Animal a: animais){
                    if(a.getId_primary()!=animais.get(0).getId_primary())
                        animais_ag_up+=",";
                    animais_ag_up+=a.getId_primary();
                }
                new ServiceDao(con_str).updateAnimaisSinc(animais_ag_up);
            }
            
            //Atualiza a flag sincronizado dos dosadores
            new ServiceDao(con_str).updateDosadoresSinc(d_sinc, d_n_sinc,"");
        } catch (Exception ex) {
            Logger.getLogger(UpdateDosador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }    
    
    /**
     * Atualiza os parametros dos dosadores que necessitam de atualização  de um por um
     * e retorna o resultado das atualizações.
     * @param dTime ArrayList com os dados a atualizar
     * @return status da atualização
     * @throws InterruptedException .
     */
    public String uDosadorTime(ArrayList<DosadorTime> dTime) throws InterruptedException{
        String status;
        String d_sinc="",d_n_sinc="";
        XbeeMsg msg_send=new XbeeMsg();
        NetworkMsgBean msg=new NetworkMsgBean();
        conexao.addObserver(msg_send);
        msg_send.conexao=conexao;             //Expecifica a conexao a ser usada
        msg.setNetwork_msg_type(MsgNetworkType.AGENTE_PARAMS_UP);
        int falhas=0;
        if(!dTime.isEmpty()){
            for(DosadorTime dt: dTime){
                msg.setDosadorTime(dt);
                //Envia a mensagem na rede e aguarda a susa resposta de entrega
                if(msg_send.sendNetworkMsgWait(dt.getDosador(), msg)!=0){
                    if(!d_n_sinc.equals(""))
                        d_n_sinc+=",";
                    d_n_sinc+=dt.getID();
                    falhas++;
                }
                if(!d_sinc.equals(""))
                    d_sinc+=",";
                d_sinc+=dt.getID();
            }
        }else{
            conexao.deleteObserver(msg_send);
            return "Nada a Atualizar";
        }
        conexao.deleteObserver(msg_send);
        if(falhas==0)
            status = "Sucesso";
        else
            status = "Nem todos os dosadores atualizados";
        try {
            new ServiceDao(con_str).updateDosadoresSinc(d_sinc, d_n_sinc,"params");
        } catch (Exception ex) {
            Logger.getLogger(UpdateDosador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }
}
